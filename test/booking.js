const chai = require('chai')
const chaiHTTP = require('chai-http')
const server = require('../index')

chai.should()

chai.use(chaiHTTP)

describe('Add a Booking', () => {
  // Adding a Booking

  describe('POST /booking/add', () => {
    const newBooking = {
      ticketId: '5fc9d959a76742e761c925f8',
      userId: '5fd0710e28b540075e6181df',
      busId: '5fc9599cbe7f0e39cd1a8485'
    }
    // success
    it('It should add a booking', (done) => {
      authToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmY2UzYjc2Njg5Njg3NDMwYTAzYTcyMyIsImVtYWlsIjoia2FqYWxhZ0BnbWFpbC5jb20iLCJpYXQiOjE2MDczNTExNzYsImV4cCI6MjkwMzM1MTE3Nn0.WtXx9fQAADsvyHkP_ylyoAV3LH7nucydDcgKiyXngCQ'

      chai.request(server).post('/booking/add').set('x-access-token', authToken).send(newBooking).end((err, response) => {
        response.should.have.status(200)
        response.body.should.be.a('object')
        response.body.should.have.property('msg')
        response.body.should.have.property('msg').eq('Seat is Booked Successfully and ticket updated')

        done()
      })
    })

    it('It should not add a booking (Same seat tried to book again)', (done) => {
      authToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmY2UzYjc2Njg5Njg3NDMwYTAzYTcyMyIsImVtYWlsIjoia2FqYWxhZ0BnbWFpbC5jb20iLCJpYXQiOjE2MDczNTExNzYsImV4cCI6MjkwMzM1MTE3Nn0.WtXx9fQAADsvyHkP_ylyoAV3LH7nucydDcgKiyXngCQ'

      chai.request(server).post('/booking/add').set('x-access-token', authToken).send(newBooking).end((err, response) => {
        response.should.have.status(400)
        response.body.should.be.a('object')
        response.body.should.have.property('msg')
        response.body.should.have.property('msg').eq('Selected Ticket is already Booked')

        done()
      })
    })
  })
})

const chai = require('chai')
const chaiHTTP = require('chai-http')
const server = require('../index')

chai.should()

chai.use(chaiHTTP)

describe('User Details Mapped with Tickets API', () => {
  // Ticket status GET API
  describe('User Details /booking/:id', () => {
    // -> SUCCESS
    it('it should return User Details of particular ticketId', (done) => {
      const ticketId = '5fc9d959a76742e761c925f8' // add a valid ticketId
      const jwtToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmYzlkODQ1OTZiNWMxNzBhZDYyODY5MCIsImVtYWlsIjoiaGFyaXNoQGdtYWlsLmNvbSIsImlhdCI6MTYwNzA2NzM4NywiZXhwIjoyOTAzMDY3Mzg3fQ.e6sOirzE0u67BJWpmi_Ot0zMM7F1GJj0IyoQWTuqzZY' // add a valid jwtToken of a user
      chai.request(server).get('/booking/' + ticketId).set('x-access-token', jwtToken).send().end((err, response) => {
        response.should.have.status(200)
        response.body.should.be.a('object')
        response.body.should.have.property('auth')
        response.body.should.have.property('data')
        response.body.should.have.property('auth').eq(true)
        done()
      })
    })

    // -> FAILURE with invalid ticketId
    it('it should not return User Details of particular ticketId-->(INVALID ticketId)', (done) => {
      const ticketId = '5fc9d9cea76742e761cd25fc' // add a invalid ticketId
      const jwtToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE2MDcwMjc5MjgsImV4cCI6MjkwMzAyNzkyOH0.kZYjNm7p7Fjm6m2P1KGBKd1GsMSYB6xHxbwmZo-mpZQ' // add a valid jwtToken of a user
      chai.request(server).get('/booking/' + ticketId).set('x-access-token', jwtToken).send().end((err, response) => {
        response.should.have.status(404)
        response.body.should.be.a('object')
        response.body.should.have.property('msg')
        response.body.should.have.property('auth')
        // response.body.should.have.property('data');
        response.body.should.have.property('msg').eq('Ticket Id is Invalid or Ticket is Open')
        response.body.should.have.property('auth').eq(true)
        // response.body.should.have.property('data').eql([]);
        done()
      })
    })

    // -> FAILURE with invalid jwtToken
    it('it should not return User Details of particular ticketId-->(INVALID jwtToken)', (done) => {
      const ticketId = '5fc9d959a76742e761c925f8' // add a valid ticketId
      const jwtToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IrpXVCJ9.eyJpYXQiOjE2MDcwMjc5MjgsImV4cCI6MjkwMzAyNzkyOH0.kZYjNm7p7Fjm6m2P1KGBKd1GsMSYB6xHxbwmZo-mpZQ' // add a invalid jwtToken of a user
      chai.request(server).get('/booking/' + ticketId).set('x-access-token', jwtToken).send().end((err, response) => {
        response.should.have.status(500)
        response.body.should.be.a('object')
        response.body.should.have.property('message')
        response.body.should.have.property('auth')
        response.body.should.have.property('token')
        response.body.should.have.property('message').eq('Failed to authenticate token.')
        response.body.should.have.property('auth').eq(false)
        response.body.should.have.property('token').eq(null)
        done()
      })
    })

    // -> FAILURE with no jwtToken
    it('it should not return User Details of particular ticketId-->(No jwtToken)', (done) => {
      const ticketId = '5fc9d9cea76742e761c925fc' // add a valid ticketId
      chai.request(server).get('/booking/' + ticketId).send().end((err, response) => {
        response.should.have.status(403)
        response.body.should.be.a('object')
        response.body.should.have.property('message')
        response.body.should.have.property('auth')
        response.body.should.have.property('token')
        response.body.should.have.property('message').eq('No token provided.')
        response.body.should.have.property('auth').eq(false)
        response.body.should.have.property('token').eq(null)
        done()
      })
    })
  })
})

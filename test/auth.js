const chai = require('chai')
const chaiHTTP = require('chai-http')
const server = require('../index')

chai.should()

chai.use(chaiHTTP)

describe('User Auth API', () => {
  // Auth POST Register API

  describe('POST /auth/register', () => {
    // success
    it('It should POST a new user details', (done) => {
      // add new details here before running mocha because of unique key values
      const newuser = {
        name: 'Kajal',
        phone: '8774513213',
        email: 'kajal@gmail.com',
        password: 'kajal'
      }
      chai.request(server).post('/auth/register').send(newuser).end((err, response) => {
        response.should.have.status(200)
        response.body.should.be.a('object')
        response.body.should.have.property('msg')
        response.body.should.have.property('msg').eq(newuser.name + '  your details registered succesfully')
        done()
      })
    })

    // failure BAD PAYLOAD ==>  phone number is not add
    it('It should not POST a new user details (BAD PAYLOAD)', (done) => {
      const newuser = {
        name: 'devnew',
        email: 'devnew@gmail.com',
        password: 'devnew'
      }
      chai.request(server).post('/auth/register').send(newuser).end((err, response) => {
        response.should.have.status(400)
        response.body.should.be.a('object')
        response.body.should.have.property('msg')
        response.body.should.have.property('msg').eq('Bad payload')
        done()
      })
    })

    // failure invalid email address
    it('It should not POST a new user details (Email validation error)', (done) => {
      const newuser = {
        name: 'devnew',
        phone: '9003330010',
        email: 'dev!@',
        password: 'devnew'
      }
      chai.request(server).post('/auth/register').send(newuser).end((err, response) => {
        response.should.have.status(404)
        response.body.should.be.a('object')
        response.body.should.have.property('msg')
        response.body.should.have.property('msg').eq('Email badly formatted')
        done()
      })
    })

    // failure Inser errors in Database
    it('It should not POST a new user details (DB Unique constraint errors/ connection errors)', (done) => {
      const newuser = {
        name: 'dev',
        phone: '9003330000',
        email: 'dev@gmail.com',
        password: 'dev'
      }
      chai.request(server).post('/auth/register').send(newuser).end((err, response) => {
        response.should.have.status(404)
        response.body.should.be.a('object')
        response.body.should.have.property('msg')
        response.body.should.have.property('msg').eq('User Details not registered, try again')
        done()
      })
    })
  })

  // Auth POST Login API

  describe('POST /auth/login', () => {
    // success
    it('It should POST, Login and provide token', (done) => {
      // add new details here before running mocha because of unique key values
      const newuser = {
        email: 'dev@gmail.com',
        password: 'dev'
      }
      chai.request(server).post('/auth/login').send(newuser).end((err, response) => {
        response.should.have.status(200)
        response.body.should.be.a('object')
        response.body.should.have.property('msg')
        response.body.should.have.property('auth')
        response.body.should.have.property('token')
        response.body.should.have.property('data')
        response.body.should.have.property('msg').eq('Login success :)')
        response.body.should.have.property('auth').eq(true)
        done()
      })
    })

    // failure BAD PAYLOAD ==>  password is not add
    it('It should not Login (BAD PAYLOAD)', (done) => {
      const newuser = {
        email: 'dev@gmail.com'
      }
      chai.request(server).post('/auth/login').send(newuser).end((err, response) => {
        response.should.have.status(400)
        response.body.should.be.a('object')
        response.body.should.have.property('msg')
        response.body.should.have.property('auth')
        response.body.should.have.property('msg').eq('Bad payload')
        response.body.should.have.property('auth').eq(false)

        done()
      })
    })

    // failure Wrong password input
    it('It should not Login (Wrong Credentials)', (done) => {
      const newuser = {
        email: 'dev@gmail.com',
        password: 'devn'
      }
      chai.request(server).post('/auth/login').send(newuser).end((err, response) => {
        response.should.have.status(404)
        response.body.should.be.a('object')
        response.body.should.have.property('msg')
        response.body.should.have.property('auth')
        response.body.should.have.property('msg').eq('Invalid Password')
        response.body.should.have.property('auth').eq(false)
        done()
      })
    })

    // failure Wrong email input
    it('It should not Login (Wrong Credentials)', (done) => {
      const newuser = {
        email: 'devn@gmail.com',
        password: 'dev'
      }
      chai.request(server).post('/auth/login').send(newuser).end((err, response) => {
        response.should.have.status(404)
        response.body.should.be.a('object')
        response.body.should.have.property('msg')
        response.body.should.have.property('auth')
        response.body.should.have.property('msg').eq('Invalid Email')
        response.body.should.have.property('auth').eq(false)
        done()
      })
    })
  })
})

const chai = require('chai')
const chaiHTTP = require('chai-http')
const server = require('../index')

chai.should()

chai.use(chaiHTTP)

describe('Admin Login API', () => {
  // Auth POST Login API
  let authToken
  describe('POST /admin/login', () => {
    // success
    it('It should POST, Admin Login and provide token', (done) => {
      // add new details here before running mocha because of unique key values
      const adminUser = {
        email: 'ad_harish@gmail.com',
        password: 'kumar'
      }
      chai.request(server).post('/admin/login').send(adminUser).end((err, response) => {
        authToken = response.body.token
        response.should.have.status(200)
        response.body.should.be.a('object')
        response.body.should.have.property('msg')
        response.body.should.have.property('auth')
        response.body.should.have.property('token')
        response.body.should.have.property('msg').eq('Admin Login success :)')
        response.body.should.have.property('auth').eq(true)
        done()
      })
    })

    // failure BAD PAYLOAD ==>  password is not add
    it('It should not Login (BAD PAYLOAD)', (done) => {
      const adminUser = {
        email: 'ad_harish@gmail.com'
      }
      chai.request(server).post('/admin/login').send(adminUser).end((err, response) => {
        response.should.have.status(403)
        response.body.should.be.a('object')
        response.body.should.have.property('msg')
        response.body.should.have.property('msg').eq('Bad payload')
        done()
      })
    })

    // failure Wrong input credentials
    it('It should not Login (Wrong Credentials)', (done) => {
      const adminUser = {
        email: 'ad_harish@gmail.com',
        password: 'devn'
      }
      chai.request(server).post('/admin/login').send(adminUser).end((err, response) => {
        response.should.have.status(404)
        response.body.should.be.a('object')
        response.body.should.have.property('msg')
        response.body.should.have.property('auth')
        response.body.should.have.property('msg').eq('Account not verified')
        response.body.should.have.property('auth').eq(false)
        done()
      })
    })
  })

  // Admin GET Reset API

  describe('GET /admin/reset', () => {
    // success
    it('It should Reset all the ticket and booking', (done) => {
      chai.request(server).get('/admin/reset').set('x-access-token', authToken).end((err, response) => {
        response.should.have.status(200)
        response.body.should.be.a('object')
        response.body.should.have.property('msg')
        response.body.should.have.property('auth')
        response.body.should.have.property('msg').eq('Both Bookings and tickets reset is done')
        response.body.should.have.property('auth').eq(true)
        done()
      })
    })

    // Incorrect token
    it('It should not reset all the ticket and booking', (done) => {
      chai.request(server).get('/admin/reset').set('x-access-token', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmY2UzYjc2Njg5Njg3NDMwYTAzYTcyMyIsImVtYWlsIjoia2FqYWxhZ0BnbWFpbC5jb20iLCJpYXQiOjE2MDczNTExNzYsImV4cCI6MjkwMzM1MTE3Nn0.WtXx9fQAADsvyHkP_ylyoAV3LH7nucydDcgKiyXngCW').end((err, response) => {
        response.should.have.status(500)
        response.body.should.be.a('object')
        response.body.should.have.property('message')
        response.body.should.have.property('auth')
        response.body.should.have.property('token')
        response.body.should.have.property('message').eq('Failed to authenticate token.')
        response.body.should.have.property('auth').eq(false)
        response.body.should.have.property('token').eq(null)
        done()
      })
    })

    // Missing token
    it('It should not reset all the ticket and booking (Token Missing)', (done) => {
      chai.request(server).get('/admin/reset').end((err, response) => {
        response.should.have.status(403)
        response.body.should.be.a('object')
        response.body.should.have.property('message')
        response.body.should.have.property('auth')
        response.body.should.have.property('token')
        response.body.should.have.property('message').eq('No token provided.')
        response.body.should.have.property('auth').eq(false)
        response.body.should.have.property('token').eq(null)
        done()
      })
    })
  })
})

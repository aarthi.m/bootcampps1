const chai = require('chai')
const chaiHttp = require('chai-http')
const ticket = require('../src/Ticket/ticketController')
chai.use(chaiHttp)
const tokenValue = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE2MDcwMjc5MjgsImV4cCI6MjkwMzAyNzkyOH0.kZYjNm7p7Fjm6m2P1KGBKd1GsMSYB6xHxbwmZo-mpZQ'

describe('Open and Closed Tickets API', () => {
  /**
     * Test GET /open Route
     */
  describe('GET /ticket/open', () => {
    it('It should GET all the Open Tickets for Valid URL', (done) => {
      chai.request(ticket)
        .get('/open')
        .set('x-access-token', tokenValue)
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.be.a('object')
          // res.body.should.have.any.property('msg','data');
          res.body.should.have.property('auth')
          done()
        })
    })

    it('It should NOT GET Open Ticket for Missing Token', (done) => {
      chai.request(ticket)
        .get('/open')
        .end((err, res) => {
          res.should.have.status(403)
          res.body.should.be.a('object')
          res.body.should.have.property('message')
          res.body.should.have.property('auth')
          done()
        })
    })

    it('It should NOT GET Open tickets for Invalid Token', (done) => {
      chai.request(ticket)
        .get('/open')
        .set('x-access-token', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9')
        .end((err, res) => {
          res.should.have.status(500)
          res.body.should.be.a('object')
          res.body.should.have.property('message')
          res.body.should.have.property('auth')
          done()
        })
    })
  })

  /**
     * Test GET /closed Route
     */

  describe('GET /ticket/closed', () => {
    it('It should GET all the Closed Tickets for Valid URL', (done) => {
      chai.request(ticket)
        .get('/closed')
        .set('x-access-token', tokenValue)
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.be.a('object')
          // res.body.should.have.property('msg');
          res.body.should.have.property('auth')
          done()
        })
    })

    it('It should NOT GET Closed Ticket for Missing Token', (done) => {
      chai.request(ticket)
        .get('/closed')
        .end((err, res) => {
          res.should.have.status(403)
          res.body.should.be.a('object')
          res.body.should.have.property('message')
          res.body.should.have.property('auth')
          done()
        })
    })

    it('It should NOT GET Closed tickets for Invalid Token', (done) => {
      chai.request(ticket)
        .get('/closed')
        .set('x-access-token', 'eyJhbGciOiJIUzI1NiIsInR5cCI6gdysXVCJ9')
        .end((err, res) => {
          res.should.have.status(500)
          res.body.should.be.a('object')
          res.body.should.have.property('message')
          res.body.should.have.property('auth')
          done()
        })
    })
  })
})

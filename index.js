const dotenv = require('dotenv')
dotenv.config()

const app = require('./src/routes')
//const connection = 
require('./models/index')

const port = process.env.PORT || 5555

app.listen(port, () => {
  console.log('Ticket Booking API is listening on  http://localhost:' + port)
})

module.exports = app

const mongoose = require('mongoose')
const dotenv = require('dotenv')
dotenv.config()

mongoose.connect(process.env.mongodbURL, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true, useFindAndModify: false }, (err) => {
  if (!err) {
    console.log('Connection Success!!')
  } else {
    console.log('Error connecting :(')
  }
})

// const userModel = require('./user.model');
// const busModel = require('./bus.model');
// const adminModel = require('./admin.model');
// const ticketModel = require('./ticket.model');

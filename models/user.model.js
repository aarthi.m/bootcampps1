const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
  userName: {
    type: String,
    required: 'Required'
  },
  userEmail: {
    type: String,
    required: 'Required',
    unique: true
  },
  userPassword: {
    type: String,
    required: 'Required'
  },
  phoneNumber: {
    type: String,
    required: 'Required',
    unique: true
  },
  createdAt: {
    type: Date,
    required: 'Required',
    default: Date.now
  }
})

mongoose.model('users', userSchema)

const express = require('express')
const router = express()
const cors = require('cors')
router.use(cors())

router.get('/harish', (req, res) => {
  res.send('Welcome to Bus ticket Booking(2020)')
})

router.use('/auth', require('./UserAuth/authController'))

router.use('/show', require('./Bus/busController'))

router.use('/admin', require('./Admin/adminController'))
router.use('/ticket', require('./Ticket/ticketController'))
router.use('/booking', require('./Booking/bookingController'))

// app.use('/admin', require('./admin/adminController'));

// app.use('/user', require('./user/userController'));

module.exports = router

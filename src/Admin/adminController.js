const express = require('express')
const admin = express.Router()

const dotenv = require('dotenv')
dotenv.config()

const bodyParser = require('body-parser')
admin.use(bodyParser.urlencoded({
  extended: false
}))
admin.use(bodyParser.json())

const mongoose = require('mongoose')

const jwt = require('jsonwebtoken')

const bcrypt = require('bcryptjs')

const jwtToken = require('./ad_jwtToken')

const adminstart = require('../../models/admin.model.js')
const adminModel = mongoose.model('admins')

const ticketstart = require('../../models/ticket.model.js')
const ticketModel = mongoose.model('tickets')

const bookingstart = require('../../models/booking.model.js')
const bookingModel = mongoose.model('bookings')

admin.post('/login', async function (req, res) {
  if (!req.body.email || !req.body.password) {
    return res.status(403).send({
      msg: 'Bad payload'
    })
  }
  await adminModel.find({ adminEmail: req.body.email }, (err, doc) => {
    if (!err) {
      if (doc.length === 0) {
        return res.status(404).send({
          auth: false,
          msg: 'User does not exist'
        })
      } else {
        const encryptedPassword = doc[0].adminPassword
        // console.log(doc[0].adminPassword, req.body.password);
        const passwordIsValid = bcrypt.compareSync(req.body.password, encryptedPassword)
        if (!passwordIsValid) {
          return res.status(404).send({
            auth: false,
            msg: 'Account not verified'
          })
        } else {
          const token = jwt.sign({
            id: doc[0].adminEmail,
            name: doc[0].adminName
          }, process.env.jwtAdminSecret, {
            expiresIn: 172800 // 2 days
          })

          return res.status(200).send({
            auth: true,
            token: token,
            msg: 'Admin Login success :)'
          })
        }
      }
    } else {
      return res.status(500).send({
        auth: false,
        msg: 'DB error'
      })
    }
  })
})

admin.get('/reset/:busId', jwtToken, async (req, res) => {
  const ticketMyQuery = { status: 'CLOSED', busId: req.params.busId }
  const ticketNewValues = { $set: { status: 'OPEN' } }
  await ticketModel.updateMany(ticketMyQuery, ticketNewValues, async function (ticketErr, ticketDoc) {
    if (!ticketErr) {
      // console.log(JSON.stringify(ticketdoc) + " data(s) updated");
      const bookingMyQuery = { busId: req.params.busId, activeCheck: true }
      const bookingNewValues = { $set: { activeCheck: false } }
      await bookingModel.updateMany(bookingMyQuery, bookingNewValues, async function (bookingErr, bookingDoc) {
        if (!bookingErr) {
          // console.log(JSON.stringify(bookingdoc) + " data(s) updated");
          return res.status(200).send({
            auth: true,
            msg: 'Both Bookings and tickets reset is done'
          })
        } else {
          return res.status(404).send({
            auth: true,
            msg: 'Only tickets reset is done and had booking error'
          })
        }
      })
    } else {
      return res.status(500).send({
        auth: true,
        msg: 'Reset unsuccessful'
      })
    }
  })
})

admin.get('/opentickets', jwtToken, async (req, res) => {
  await ticketModel.find({ status: 'OPEN' }, (err, doc) => {
    if (!err) {
      if (doc.length === 0) {
        return res.status(404).send({
          auth: true,
          msg: 'No Open tickets'
        })
      } else {
        const tickets = []
        for (let i = 0; i < doc.length; i++) {
          const data = {
            ticketId: doc[i]._id,
            busId: doc[i].busId,
            seatNo: doc[i].seatNo,
            status: doc[i].status
          }
          tickets.push(data)
        }
        const d = {
          tickets: tickets
        }
        // console.log(String(Date()));
        return res.status(200).send({
          auth: true,
          data: d
        })
      }
    } else {
      return res.status(500).send({
        auth: true,
        msg: 'DB error'
      })
    }
  })
})

// Get Closed Tickets
admin.get('/closedtickets', jwtToken, async (req, res) => {
  await ticketModel.find({ status: 'CLOSED' }, (err, doc) => {
    if (!err) {
      if (doc.length === 0) {
        return res.status(200).send({
          auth: true,
          msg: 'No Closed tickets'
        })
      } else {
        const tickets = []
        for (let i = 0; i < doc.length; i++) {
          const data = {
            ticketId: doc[i]._id,
            busId: doc[i].busId,
            seatNo: doc[i].seatNo,
            status: doc[i].status
          }
          tickets.push(data)
        }
        const d = {
          tickets: tickets
        }
        // console.log(String(Date()));
        return res.status(200).send({
          auth: true,
          data: d
        })
      }
    } else {
      return res.status(500).send({
        auth: true,
        msg: 'DB error'
      })
    }
  })
})

admin.get('/resetall', jwtToken, async (req, res) => {
  const ticketMyQuery = { status: 'CLOSED' }
  const ticketNewValues = { $set: { status: 'OPEN' } }
  await ticketModel.updateMany(ticketMyQuery, ticketNewValues, async function (ticketErr, ticketDoc) {
    if (!ticketErr) {
      // console.log(JSON.stringify(ticketdoc) + " data(s) updated");
      const bookingMyQuery = { activeCheck: true }
      const bookingNewValues = { $set: { activeCheck: false } }
      await bookingModel.updateMany(bookingMyQuery, bookingNewValues, async function (bookingErr, bookingDoc) {
        if (!bookingErr) {
          // console.log(JSON.stringify(bookingdoc) + " data(s) updated");
          return res.status(200).send({
            auth: true,
            msg: 'Both Bookings and tickets reset is done'
          })
        } else {
          return res.status(404).send({
            auth: true,
            msg: 'Only tickets reset is done and had booking error'
          })
        }
      })
    } else {
      return res.status(500).send({
        auth: true,
        msg: 'Reset unsuccessful'
      })
    }
  })
})

module.exports = admin

const express = require('express')
const bus = express()
const bodyParser = require('body-parser')

const dotenv = require('dotenv')
dotenv.config()
const adminjwtToken = require('../Admin/ad_jwtToken')

const mongoose = require('mongoose')

// eslint-disable-next-line no-unused-vars
const connection = require('../../models/bus.model.js')
const busModel = mongoose.model('buses')
const connectionTicket = require('../../models/ticket.model.js')
const ticketModel = mongoose.model('tickets')
const connectionModel = require('../../models/booking.model.js')
const bookingModel = mongoose.model('bookings')

bus.use(bodyParser.urlencoded({
  extended: false
}))
bus.use(bodyParser.json())

bus.get('/', async (req, res) => {
  await busModel.find((err, doc) => {
    if (!err) {
      if (doc.length === 0) {
        return res.status(404).send({
          auth: true,
          msg: 'No Data'
        })
      } else {
        const buses = []
        for (let i = 0; i < doc.length; i++) {
          const data = {
            busId: doc[i]._id,
            busName: doc[i].busName,
            busTiming: doc[i].busTiming,
            busFromTo: doc[i].busFromTo,
            active: doc[i].activeCheck,
            noOfTickets: doc[i].noOfTickets
          }
          buses.push(data)
        }
        const d = {
          Buses: buses
        }
        // console.log(String(Date()));
        return res.status(200).send({
          auth: true,
          data: d
        })
      }
    } else {
      return res.status(500).send({
        auth: true,
        msg: "Bus Details couldn't be fetched. Please try again"
      })
    }
  })
})

bus.post('/add', adminjwtToken, async (req, res) => {
  if (!req.body.name || !req.body.timing || !req.body.fromTo || !req.body.noOfTickets) {
    return res.status(400).send({
      msg: 'Bad payload'
    })
  }

  const busReg = new busModel()
  busReg.busName = req.body.name
  busReg.busTiming = req.body.timing
  busReg.busFromTo = req.body.fromTo
  busReg.noOfTickets = req.body.noOfTickets
  busReg.activeCheck = true
  busReg.createdAt = new Date()
  await busReg.save((busErr, busDoc) => {
    if (!busErr && busDoc !== null) {
      const tickets = []
      for (let i = 1; i <= req.body.noOfTickets; i++) {
        const newTicket = new ticketModel()
        newTicket.busId = busDoc._id
        newTicket.seatNo = `S${i}`
        newTicket.status = 'OPEN'
        tickets.push(newTicket)
      }
      ticketModel.insertMany(tickets, (ticketErr, ticketDoc) =>{
        if (!ticketErr && ticketDoc !== null) {
          return res.status(200).send({
            msg: 'Bus and Tickets Added Successfully'
          })
        } else {
          return res.status(404).send({
            msg: 'Bus Added succesfully but Tickets Not Added'
          })
        }
      })
    } else {
      return res.status(404).send({
        msg: 'Bus not Added, try again'
      })
    }
  })
})

bus.delete('/deletebus/:id', adminjwtToken, async (req, res) => {
  await busModel.deleteOne({ _id: req.params.id }, async (busErr, busDoc) => {
    if (!busErr) {
      await ticketModel.deleteMany({ busId: req.params.id }, async (ticketErr, ticketDoc) => {
        if (!ticketErr) {
          await bookingModel.updateMany({ busId: req.params.id, activeCheck: true }, { $set: { activeCheck: false } }, async (bookingErr, bookingDoc) => {
            if (!bookingErr) {
              return res.status(200).send({
                auth: true,
                msg: 'Both Bus and the corresponding tickets removed successfully and Bookings set to inactive.'
              })
            } else {
              return res.status(404).send({
                auth: true,
                msg: 'Both Bus and the corresponding tickets removed successfully but tickets not set to inactive.'
              })
            }
          })
        } else {
          return res.status(404).send({
            auth: true,
            msg: 'Bus deleted but Tickets not deleted'
          })
        }
      })
    } else {
      return res.status(500).send({
        auth: true,
        msg: 'Bus not deleted. Please try again'
      })
    }
  })
})

module.exports = bus
